﻿using Microsoft.AspNetCore.Identity;
using System.Data.Common;

namespace WebApp.Data.DbModel
{
    public class ApplicationUser : IdentityUser
    {
        public string? Address { get; set; }
    }
}
