﻿namespace WebApp.Enum
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}