﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data.DbModel;
using WebApp.Service.Interface;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [AutoValidateAntiforgeryToken]
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                var students = _studentService.GetAllStudents();
                return View(students);
            }
            catch (Exception ex)
            {
                return Content("Error Occured : " + ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Student student, IFormFile image)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_studentService.AddStudent(student,image))
                    {
                        return RedirectToAction("index", "student");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Failed to Add Item");
                        return View(student);
                    }
                }
                else
                {
                    return View(student);
                }
            }
            catch (Exception ex)
            {
                return Content("Error Occured : " + ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            try
            {
                var student = _studentService.GetStudentById(id);
                if (student != null)
                {
                    return View(student);
                }
                else
                {
                    return Content("Item Not Found");
                }
            }
            catch (Exception ex)
            {
                return Content("Error Occured : " + ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_studentService.UpdateStudent(student))
                    {
                        return RedirectToAction("index", "student");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Failed to Edit Item");
                        return View(student);
                    }
                }
                else
                {
                    return View(student);
                }
            }
            catch (Exception ex)
            {
                return Content("Error Occured : " + ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            try
            {
                if (_studentService.DeleteStudent(id))
                {
                    return RedirectToAction("index", "student");
                }
                else
                {
                    return Content("Item Not Found");
                }
            }
            catch (Exception ex)
            {
                return Content("Error Occured : " + ex.Message);
            }
        }
    }
}