﻿using WebApp.Data.DbModel;
using WebApp.Data.Repository.Interface;
using WebApp.Module.Interface;
using WebApp.Service.Interface;

namespace WebApp.Service.Implementation
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IFileHelper _fileHelper;

        public StudentService(IStudentRepository studentRepository, IFileHelper fileHelper)
        {
            _studentRepository = studentRepository;
            _fileHelper = fileHelper;
        }

        public bool AddStudent(Student student,IFormFile file)
        {
            var a = _fileHelper.SaveFileAndReturnName("img", file);
            if (_studentRepository.Insert(student) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateStudent(Student student)
        {
            if (_studentRepository.Update(student) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteStudent(Guid id)
        {
            var student = _studentRepository.Find(id);
            if (student != null)
            {
                if (_studentRepository.Delete(student) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Student? GetStudentById(Guid id)
        {
            return _studentRepository.Find(id);
        }

        public List<Student> GetAllStudents()
        {
            return _studentRepository.List();
        }

        public List<Student> GetByName(string name)
        {
            return _studentRepository.GetQueryable().Where(x => x.Name == name).ToList();
        }

        public List<int> GetAllRollNo()
        {
            return _studentRepository.GetQueryable().Select(x => x.Roll).ToList();
        }
    }
}